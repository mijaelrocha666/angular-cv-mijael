import { Injectable } from '@angular/core';
import { elementAt } from 'rxjs';
import { IDatos } from '../interfaces/citas';

@Injectable({
  providedIn: 'root'
})
export class CitaService {
  LISTA_CITAS: IDatos[] = [

  ];
  constructor() { }

  getCitas(){
    return this.LISTA_CITAS.slice();
  }

  agregarCita(cita:IDatos){
    this.LISTA_CITAS.push(cita);
  }

  eliminarCita(nombre:string){
    this.LISTA_CITAS=this.LISTA_CITAS.filter(data =>{
      return data.nombre !== nombre;
    })
  }

  modificarCita(names:IDatos){
    this.eliminarCita(names.nombre);
    this.agregarCita(names);
  }

  buscarCita(id:string):IDatos{
    return this.LISTA_CITAS.find(element => element.nombre === id) || {} as IDatos;
  }
}
